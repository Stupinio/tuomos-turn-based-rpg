﻿using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class ObjectLayoutGroup : MonoBehaviour
{
    public FillType fillType = FillType.Grow;
    public bool showDebugLines = true;

    [Header("Row")]
    public float rowLength = 5f;
    public Vector3 rowDirection = Vector3.forward;

    [Header("Rows")]
    public int objectsPerRow = 4;
    public Vector3 extraRowsDirection = -Vector3.right;
    public float rowSpacing = 1.5f;

    List<Transform> objects = new List<Transform>();
    int childCount = 0;

    void Update()
    {
        if (HasChanged())
            Refresh();
    }

    void OnValidate()
    {
        Refresh();
    }

    bool HasChanged()
    {
        int _childCount = transform.childCount;

        if (_childCount == childCount)
            return false;
        else
        {
            childCount = _childCount;
            return true;
        }
    }

	public void Refresh()
	{
        if (rowDirection == Vector3.zero || extraRowsDirection == Vector3.zero)
        {
            Debug.LogWarning("Directions can't be (0, 0, 0)");
            return;
        }

        rowDirection.Normalize();
        extraRowsDirection.Normalize();

        int _childCount = transform.childCount;
        if (_childCount == 0)
            return;

        float _childSpacing = CalculateChildSpacing(Mathf.Min(_childCount, objectsPerRow));
        
        objects.Clear();

        int _currentRow = 0;
        int _objIndex = 0;
        for (int i = 0; i < _childCount; i++)
        {
            objects.Add(transform.GetChild(i));
            Vector3 _objPos = (_objIndex != 0) ? rowDirection * (_objIndex * _childSpacing) : Vector3.zero;
            Vector3 _rowPos = (_currentRow != 0) ? extraRowsDirection * _currentRow * rowSpacing : Vector3.zero;

            objects[i].localPosition = _objPos + _rowPos;

            if (_objIndex == objectsPerRow - 1)
            {
                _childSpacing = CalculateChildSpacing(Mathf.Min(_childCount - (objectsPerRow * (_currentRow + 1)), objectsPerRow));
                _objIndex = 0;
                _currentRow++;
            }
            else
                _objIndex++;
        }
	}

    float CalculateChildSpacing(int _objectsOnCurrentRow)
    {
        if (fillType == FillType.Grow && _objectsOnCurrentRow < objectsPerRow)
            return (rowLength / (objectsPerRow - 1));
        else
            return (rowLength / Mathf.Min(_objectsOnCurrentRow - 1, objectsPerRow - 1));
    }

    void OnDrawGizmosSelected()
    {
        if (showDebugLines == false)
            return;

        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + rowDirection * rowLength);
        Gizmos.DrawLine(transform.position + extraRowsDirection * rowSpacing, transform.position + (extraRowsDirection * rowSpacing) + (rowDirection * rowLength));
        Gizmos.DrawLine(transform.position + extraRowsDirection * (rowSpacing * 2f), transform.position + (extraRowsDirection * (rowSpacing * 2f)) + (rowDirection * rowLength));
    }

    public enum FillType { Grow, Stretched }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollisionCaster : MonoBehaviour
{
    #region Singleton
    public static CollisionCaster Instance { get; private set; }
    void Awake()
    {
        Instance = this;
    }
    #endregion
    public LayerMask raycastMask;
    public Vector3 collisionBoxSize;
    public float rayMinSpacing = 0.25f;
    public float rayLength = 1f;
    public float padding = 0.1f;

    public CollisionCheck ForwardCollision { get; private set; }
    public CollisionCheck BackwardCollision { get; private set; }
    public CollisionCheck LeftCollision { get; private set; }
    public CollisionCheck RightCollision { get; private set; }
    public CollisionCheck UpCollision { get; private set; }
    public CollisionCheck DownCollision { get; private set; }

    [Header("Debugging")]
    public bool debug = false;
    public Text debugTextObj;
    public bool drawRays = false;
    public bool drawCube = false;

    const int maxRayCount = 50;

    void Start()
    {
        ForwardCollision = new CollisionCheck();
        BackwardCollision = new CollisionCheck();
        LeftCollision = new CollisionCheck();
        RightCollision = new CollisionCheck();
        UpCollision = new CollisionCheck();
        DownCollision = new CollisionCheck();
    }

	void Update()
	{
        CheckCollisions();

        if (debug)
            DebugCollisions(debugTextObj);
    }

    void ResetCollisions()
    {
        LeftCollision.hit = false;
        RightCollision.hit = false;
        UpCollision.hit = false;
        DownCollision.hit = false;
        BackwardCollision.hit = false;
        ForwardCollision.hit = false;
    }

    void CheckCollisions()
    {
        ResetCollisions();

        int _rayCountY = Mathf.Clamp(Mathf.FloorToInt(collisionBoxSize.y / rayMinSpacing), 1, maxRayCount);
        int _rayCountX = Mathf.Clamp(Mathf.FloorToInt(collisionBoxSize.x / rayMinSpacing), 1, maxRayCount);
        int _rayCountZ = Mathf.Clamp(Mathf.FloorToInt(collisionBoxSize.z / rayMinSpacing), 1, maxRayCount);
        float _raySpacingY = (collisionBoxSize.y) / _rayCountY;
        float _raySpacingX = (collisionBoxSize.x) / _rayCountX;
        float _raySpacingZ = (collisionBoxSize.z) / _rayCountZ;

        for (int y = 0; y <= _rayCountY; y++)
        {
            Vector3 _bottomLeftBackCorner = transform.position + (transform.up * (_raySpacingY * y)) - (transform.right * (collisionBoxSize.x * 0.5f)) - (transform.forward * (collisionBoxSize.z * 0.5f));

            for (int x = 0; x <= _rayCountX; x++)
            {
                Vector3 _startPosX = _bottomLeftBackCorner + (transform.right * (_raySpacingX * x));

                for (int z = 0; z <= _rayCountZ; z++)
                {
                    Vector3 _startPosZ = _bottomLeftBackCorner + (transform.forward * (_raySpacingZ * z));

                    RaycastHit _hitInfo;

                    if (Physics.Raycast(_startPosZ + (transform.right * padding), -transform.right, out _hitInfo, rayLength, raycastMask))
                    {
                        if (LeftCollision.hit == false || _hitInfo.distance - padding < LeftCollision.distance)
                        {
                            LeftCollision.hit = true;
                            LeftCollision.distance = _hitInfo.distance - padding;
                        }
                    }

                    RaycastHit _hitInfo2;
                    if (Physics.Raycast(_startPosZ + transform.right * (collisionBoxSize.x - padding), transform.right, out _hitInfo2, rayLength, raycastMask))
                    {
                        if (RightCollision.hit == false || _hitInfo2.distance - padding < RightCollision.distance)
                        {
                            RightCollision.hit = true;
                            RightCollision.distance = _hitInfo2.distance - padding;
                        }
                    }

                    if (Physics.Raycast(_startPosX + (transform.forward * padding), -transform.forward, out _hitInfo, rayLength, raycastMask))
                    {
                        if (BackwardCollision.hit == false || _hitInfo.distance - padding < BackwardCollision.distance)
                        {
                            BackwardCollision.hit = true;
                            BackwardCollision.distance = _hitInfo.distance - padding;
                        }
                    }

                    if (Physics.Raycast(_startPosX + transform.forward * (collisionBoxSize.z - padding), transform.forward, out _hitInfo, rayLength, raycastMask))
                    {
                        if (ForwardCollision.hit == false || _hitInfo.distance - padding < ForwardCollision.distance)
                        {
                            ForwardCollision.hit = true;
                            ForwardCollision.distance = _hitInfo.distance - padding;
                        }
                    }

                    if (y == 0)
                    {
                        if (Physics.Raycast(_startPosZ + (transform.right * _raySpacingX * x) + (transform.up * padding), -transform.up, out _hitInfo, rayLength, raycastMask))
                        {
                            if (DownCollision.hit == false || _hitInfo.distance - padding < DownCollision.distance)
                            {
                                DownCollision.hit = true;
                                DownCollision.distance = _hitInfo.distance - padding;
                            }
                        }
                    }
                    else if (y == _rayCountY)
                    {
                        if (Physics.Raycast(_startPosZ + (transform.right * _raySpacingX * x) - (transform.up * padding), transform.up, out _hitInfo, rayLength, raycastMask))
                        {
                            if (UpCollision.hit == false || _hitInfo.distance - padding < UpCollision.distance)
                            {
                                UpCollision.hit = true;
                                UpCollision.distance = _hitInfo.distance - padding;
                            }
                        }
                    }
                }
            }
        }
    }

    public Vector3 ModifyMovementByCollisions(Vector2 _input, Vector3 _movement)
    {
        Vector3 _pos = transform.position + _movement;

        if (_input.y > 0f && ForwardCollision.hit)
            _movement.z = Mathf.Clamp(_movement.z, 0f, ForwardCollision.distance);

        if (_input.y < 0f && BackwardCollision.hit)
            _movement.z = Mathf.Clamp(_movement.z, -BackwardCollision.distance, 0f);

        if (_input.x > 0f && RightCollision.hit)
            _movement.x = Mathf.Clamp(_movement.x, 0f, RightCollision.distance);
        
        if (_input.x < 0f && LeftCollision.hit)
            _movement.x = Mathf.Clamp(_movement.x, -LeftCollision.distance, 0f);

        return _movement;
    }

    void DebugCollisions(Text _textObj)
    {
        _textObj.text = "Up: " + ((UpCollision.hit) ? UpCollision.distance.ToString("F3") : "") +
                        "\nDown: " + ((DownCollision.hit) ? DownCollision.distance.ToString("F3") : "") +
                        "\nLeft: " + ((LeftCollision.hit) ? LeftCollision.distance.ToString("F3") : "") +
                        "\nRight: " + ((RightCollision.hit) ? RightCollision.distance.ToString("F3") : "") +
                        "\nBack: " + ((BackwardCollision.hit) ? BackwardCollision.distance.ToString("F3") : "") +
                        "\nForward: " + ((ForwardCollision.hit) ? ForwardCollision.distance.ToString("F3") : "");
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green * 0.3f;

        // Draw box
        if (drawCube)
        {
            Vector3 _center = transform.position;
            _center.y += collisionBoxSize.y / 2f;
            Gizmos.DrawWireCube(_center, collisionBoxSize);
            Gizmos.DrawWireCube(_center, new Vector3(collisionBoxSize.x - padding * 2f, collisionBoxSize.y - padding * 2f, collisionBoxSize.z - padding * 2f));
        }

        // Draw horizontal rays
        if (drawRays)
        {
            int _rayCountY = Mathf.Clamp(Mathf.FloorToInt(collisionBoxSize.y / rayMinSpacing), 1, maxRayCount);
            int _rayCountX = Mathf.Clamp(Mathf.FloorToInt(collisionBoxSize.x / rayMinSpacing), 1, maxRayCount);
            int _rayCountZ = Mathf.Clamp(Mathf.FloorToInt(collisionBoxSize.z / rayMinSpacing), 1, maxRayCount);
            float _raySpacingY = (collisionBoxSize.y) / _rayCountY;
            float _raySpacingX = (collisionBoxSize.x) / _rayCountX;
            float _raySpacingZ = (collisionBoxSize.z) / _rayCountZ;

            for (int y = 0; y <= _rayCountY; y++)
            {
                Vector3 _bottomLeftBackCorner = transform.position + (transform.up * (_raySpacingY * y)) - (transform.right * (collisionBoxSize.x * 0.5f)) - (transform.forward * (collisionBoxSize.z * 0.5f));

                for (int x = 0; x <= _rayCountX; x++)
                {
                    Vector3 _startPosX = _bottomLeftBackCorner + (transform.right * (_raySpacingX * x));

                    for (int z = 0; z <= _rayCountZ; z++)
                    {
                        Vector3 _startPosZ = _bottomLeftBackCorner + (transform.forward * (_raySpacingZ * z));

                        Gizmos.DrawLine(_startPosZ + (transform.right * padding), _startPosZ - transform.right * rayLength);
                        Gizmos.DrawLine(_startPosZ + transform.right * (collisionBoxSize.x - padding), _startPosZ + transform.right * rayLength + transform.right * collisionBoxSize.x);

                        Gizmos.DrawLine(_startPosX + (transform.forward * padding), _startPosX - transform.forward * rayLength);
                        Gizmos.DrawLine(_startPosX + transform.forward * (collisionBoxSize.z - padding), _startPosX + transform.forward * rayLength + transform.forward * collisionBoxSize.z);

                        if (y == 0)
                        {
                            Gizmos.DrawLine(_startPosZ + (transform.right * _raySpacingX * x) + (transform.up * padding), _startPosZ + (transform.right * _raySpacingX * x) - transform.up * rayLength);
                        }
                        else if (y == _rayCountY)
                        {
                            Gizmos.DrawLine(_startPosZ + (transform.right * _raySpacingX * x) - (transform.up * padding), _startPosZ + (transform.right * _raySpacingX * x) + transform.up * rayLength);
                        }
                    }
                }
            }
        }
    }

    public class CollisionCheck
    {
        public bool hit = false;
        public float distance = 0f;
    }

    public static Vector3 RotateVectorAroundAxis(Vector3 _vector, Vector3 _aroundAxis, float _degrees)
    {
        Vector3 test = Vector3.Cross(_vector, _aroundAxis);
        if (test == Vector3.zero)
        {
            return _vector;
        }
        Vector3 parallelToAroundAxis = Vector3.Dot(_vector, _aroundAxis.normalized) * _aroundAxis.normalized;
        Vector3 perpendicularToAroundAxis = _vector - parallelToAroundAxis;
        Vector3 thirdVector = Vector3.Cross(_aroundAxis.normalized, perpendicularToAroundAxis).normalized;
        Vector3 result = parallelToAroundAxis + Mathf.Cos(_degrees) * perpendicularToAroundAxis + (perpendicularToAroundAxis.magnitude) * Mathf.Sin(_degrees) * thirdVector;
        return result;
    }
}
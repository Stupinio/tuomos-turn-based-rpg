﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuManager : MonoBehaviour
{
    public Player[] player;
    public Enemy[] enemy;
    public int numberOfPlayers = 4;
    public int index = 0;
    public GameObject targetlist;

    //GameObject hpPlayerGO;
    //Text hpPlayer;
    //GameObject damagePlayerGO;
    //Text damagePlayer;

    //GameObject hpEnemyGO;
    //Text hpEnemy;
    //GameObject damageEnemyGO;
    //Text damageEnemy;

    //GameObject textPlayerGO;
    //Text textPlayer;



    // Start is called before the first frame update
    void Start()
    {
        HpUpdate();

        player[index].textPlayerGO.GetComponent<Text>().text = player[index].name;

        //textPlayerGO = GameObject.Find("TextPlayer");
        //textPlayer = textPlayerGO.GetComponent<Text>();
        //textPlayer.text = player[index].name;
    }

    // Update is called once per frame

    public void SaveButtonPressed(GameObject buttonTosave)
    {
        player[index].buttonPressed = buttonTosave;
    }

    public void Attacking(Enemy enemy)
    {
        if (enemy != null)
        {
            player[index].enemy = enemy;
            player[index].Move(player[index].buttonPressed.name);
            StartCoroutine(Wait());
        }
        else
        {
            Menu.CloseMenu();

            index = (index + 1) % numberOfPlayers;
            player[index].textPlayerGO.GetComponent<Text>().text = player[index].name;

            Menu.OpenInitialMenu();

            player[index].textPlayerGO.SetActive(true);
        }
    }

    IEnumerator Wait()
    {
        Menu.CloseMenu();

        player[index].textPlayerGO.SetActive(false);

        yield return new WaitForSeconds(1.1f);
        //player[index].enemy.damageEnemyGO.SetActive(true);
        StartCoroutine(HpUpdate(index));
        StartCoroutine(DamageUpdate(index));
        StartCoroutine(TargetlistUpdate());

        yield return new WaitForSeconds(0.4f);

        index = (index + 1) % numberOfPlayers;
        player[index].textPlayerGO.GetComponent<Text>().text = player[index].name;
        yield return new WaitForSeconds(0.5f);

        Menu.OpenInitialMenu();

        player[index].textPlayerGO.SetActive(true);
    }

    IEnumerator HpUpdate(int index)
    {
        //switch(index)
        //{
        //    case 0:
        //        hpPlayerGO = GameObject.Find("TextPlayer1");
        //        hpPlayer = hpPlayerGO.GetComponent<Text>();
        //        hpEnemyGO = GameObject.Find("TextEnemy1");
        //        hpEnemy = hpEnemyGO.GetComponent<Text>();
        //        break;
        //    case 1:
        //        hpPlayerGO = GameObject.Find("TextPlayer2");
        //        hpPlayer = hpPlayerGO.GetComponent<Text>();
        //        hpEnemyGO = GameObject.Find("TextEnemy2");
        //        hpEnemy = hpEnemyGO.GetComponent<Text>();
        //        break;
        //    case 2:
        //        hpPlayerGO = GameObject.Find("TextPlayer3");
        //        hpPlayer = hpPlayerGO.GetComponent<Text>();
        //        hpEnemyGO = GameObject.Find("TextEnemy3");
        //        hpEnemy = hpEnemyGO.GetComponent<Text>();
        //        break;
        //    case 3:
        //        hpPlayerGO = GameObject.Find("TextPlayer4");
        //        hpPlayer = hpPlayerGO.GetComponent<Text>();
        //        hpEnemyGO = GameObject.Find("TextEnemy4");
        //        hpEnemy = hpEnemyGO.GetComponent<Text>();
        //        break;
        //}
        player[index].hpPlayerGO.GetComponent<Text>().text = player[index].health.ToString();
        player[index].enemy.hpEnemyGO.GetComponent<Text>().text = player[index].enemy.health.ToString();

        yield return null;
    }

    IEnumerator DamageUpdate(int index)
    {
        //switch (index)
        //{
        //    case 0:
        //        //hpPlayerGO = GameObject.Find("DamagePlayer1");
        //        //hpPlayer = hpPlayerGO.GetComponent<Text>();
        //        damageEnemyGO = GameObject.Find("DamageEnemy1");
        //        damageEnemy = damageEnemyGO.GetComponent<Text>();
        //        break;
        //    case 1:
        //        //hpPlayerGO = GameObject.Find("DamagePlayer2");
        //        //hpPlayer = hpPlayerGO.GetComponent<Text>();
        //        damageEnemyGO = GameObject.Find("DamageEnemy2");
        //        damageEnemy = damageEnemyGO.GetComponent<Text>();
        //        break;
        //    case 2:
        //        //hpPlayerGO = GameObject.Find("TextPlayer3");
        //        //hpPlayer = hpPlayerGO.GetComponent<Text>();
        //        damageEnemyGO = GameObject.Find("DamageEnemy3");
        //        damageEnemy = damageEnemyGO.GetComponent<Text>();
        //        break;
        //    case 3:
        //        //hpPlayerGO = GameObject.Find("DamagePlayer4");
        //        //hpPlayer = hpPlayerGO.GetComponent<Text>();
        //        damageEnemyGO = GameObject.Find("DamageEnemy4");
        //        damageEnemy = damageEnemyGO.GetComponent<Text>();
        //        break;
        //}
        //hpPlayer.text = player[index].health.ToString();
        player[index].enemy.damageEnemyGO.GetComponent<Text>().text = player[index].damage.ToString();
        yield return new WaitForSeconds(0.9f);
        player[index].enemy.damageEnemyGO.GetComponent<Text>().text = "";
    }

    IEnumerator TargetlistUpdate()
    {
        //Button[] buttons = targetlist.GetComponents<Button>();
        foreach (Enemy fiend in enemy)
        {
            if (fiend != null)
            {
                //Debug.Log(fiend.name);
                if (fiend.dead == true)
                {
                    //Debug.Log("päästiin tänne");
                    //Debug.Log(fiend.name);
                    GameObject.Destroy(targetlist.transform.Find(fiend.name).gameObject);
                    GameObject.Destroy(fiend.gameObject);
                }
            }
            
        }
        yield return null;
    }

    public void HpUpdate()
    {
        int i = 0;
        while (i != 4)
        {
            StartCoroutine(HpUpdate(i));
            i++;
        }
    }

    public void Skip()
    {
        index = (index + 1) % numberOfPlayers;
        player[index].textPlayerGO.GetComponent<Text>().text = player[index].name;
    }

    public void HealEnemies()
    {
        foreach (Enemy fiend in enemy)
        {
            fiend.health = fiend.BaseHealth();
        }
        HpUpdate();
        Menu.CloseMenu();
        Menu.OpenInitialMenu();
        //activeMenu.SetActive(false);
        //activeMenu = menuInitial;
        //activeMenu.SetActive(true);
        index = (index + 1) % numberOfPlayers;
        player[index].textPlayerGO.GetComponent<Text>().text = player[index].name;

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public interface ICombatant
{
    void TakeDamage(int damage);
}


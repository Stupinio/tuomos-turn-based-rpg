﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enemy : Combatant
{
    public int damage;

    public GameObject textEnemyGO;
    public GameObject hpEnemyGO;
    public GameObject damageEnemyGO;

    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        damageEnemyGO.GetComponent<Text>().text = "";
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected override void Die()
    {
        dead = true;
        //textEnemyGO.SetActive(false);
        hpEnemyGO.SetActive(false);
        damageEnemyGO.SetActive(false);
        //GameObject.Destroy(gameObject);
    }
}

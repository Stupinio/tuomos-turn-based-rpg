﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Combatant : MonoBehaviour, ICombatant
{
    public int baseHealth;
    public int baseStrength;
    public int baseMagic;
    public int baseDefence;
    public int baseMagicDefence;

    public enum Stat { strength, magic, none }

    public int health;
    public int strength;
    public int magic;
    public int defence;
    public int magicDefence;

    public Stat damageType;
    public bool dead;

    // Start is called before the first frame update
    protected virtual void Start()
    {
        health = baseHealth;
        strength = baseStrength;
        magic = baseMagic;
        defence = baseDefence;
        magicDefence = baseMagicDefence;
        damageType = Stat.none;
    }

    public void TakeDamage(int damage)
    {
        switch (damageType)
        {
            case Stat.strength:
                Debug.Log("TakeDamage Strength");
                health -= Mathf.Max(damage - defence, 0);
                break;
            case Stat.magic:
                Debug.Log("TakeDamage Magic");
                health -= Mathf.Max(damage - magicDefence, 0);
                break;
            case Stat.none:
                break;
            default:
                Debug.Log("TakeDamage Default");
                health -= damage;
                break;
        }

        if (health <= 0 && !dead)
        {
            Debug.Log("Die activated");
            Die();
        }
    }

    protected virtual void Die()
    {
        dead = true;
        GameObject.Destroy(gameObject);
    }

    public int BaseHealth()
    {
        return baseHealth;
    }
}

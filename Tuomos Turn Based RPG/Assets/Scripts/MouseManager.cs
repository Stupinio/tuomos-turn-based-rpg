﻿using UnityEngine;

public class MouseManager : MonoBehaviour
{
	#region Singleton
	public static MouseManager Instance { get; private set; }
	void Awake()
	{
		Instance = this;
	}
    #endregion
    public Camera rayCamera;
    public LayerMask rayMask;
    public float rayLength = 100f;

	//void Update()
	//{
 //       if (Input.GetMouseButtonDown(0))
 //       {
 //           VisualizerListener _listener;
 //           if (GetComponentUnderMouse(out _listener, true))
 //           {
 //               Debug.Log("Found: " + _listener.transform.name);
 //           }
 //       }
	//}

    public static T GetComponentUnderMouse<T>(bool _checkParent = false)
    {
        Ray _ray = Instance.rayCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit _hitInfo;
        if (Physics.Raycast(_ray, out _hitInfo, Instance.rayLength, Instance.rayMask))
        {
            T _component = _hitInfo.collider.GetComponent<T>();
            if (_component != null)
                return _component;

            if (_checkParent)
            {
                _component = _hitInfo.collider.GetComponentInParent<T>();
                if (_component != null)
                    return _component;
            }
        }

        return default(T);
    }

    public static bool GetComponentUnderMouse<T>(out T _foundComponent, bool _checkParent = false)
    {
        _foundComponent = GetComponentUnderMouse<T>(_checkParent);

        return (_foundComponent != null);
    }
}
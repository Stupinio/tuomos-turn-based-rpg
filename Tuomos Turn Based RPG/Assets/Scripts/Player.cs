﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : Combatant
{
    //public int direction = 0;
    public Enemy enemy;
    public Color activeStatus = Color.white;
    public Renderer rend;
    public Color intialStatus;

    public GameObject textPlayerGO;
    public GameObject hpPlayerGO;
    public GameObject damagePlayerGO;


    public GameObject buttonPressed;

    public int damage;
    // Start is called before the first frame update
    protected override void Start()
    {
        base.Start();
        rend = GetComponent<Renderer>();
        intialStatus = rend.material.color;
        damage = strength;
        damagePlayerGO.GetComponent<Text>().text = "";
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Move(string buttonPressed)
    {
        //int damage = strength;

        switch (buttonPressed)
        {
            case "ButtonMele":
                enemy.damageType = Stat.strength;
                damage = strength;
                break;
            case "ButtonThunder":
                enemy.damageType = Stat.magic;
                damage = magic;
                break;
            case "ButtonPunch":
                enemy.damageType = Stat.strength;
                damage = defence;
                break;
            case "ButtonFlame":
                enemy.damageType = Stat.magic;
                damage = magicDefence;
                break;
            case "ButtonWall":
                enemy.damageType = Stat.none;
                defence += 5;
                magicDefence += 5;
                damage = 0;
                break;
            default:
                enemy.damageType = Stat.none;
                damage = 0;
                break;
        }

        Vector3 distance = new Vector3(2.5f, 0, 0);
        //move smoothly and wait a while
        StartCoroutine(Moving(distance, damage));
    }

    IEnumerator Moving(Vector3 distance, int damage)
    {
        rend.material.color = activeStatus;
        int i = 0;
        while (i != 60)
        {
            transform.position = Vector3.Lerp(transform.position, transform.position + distance, Time.deltaTime);
            yield return null;
            i++;
        }
        enemy.TakeDamage(damage);
        while (i != 0)
        {
            transform.position = Vector3.Lerp(transform.position, transform.position - distance, Time.deltaTime);
            yield return null;
            i--;
        }
        rend.material.color = intialStatus;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Menu : MonoBehaviour
{
    public Menu parentMenu;

    static Menu currentMenu;

    static Menu menuInitial;

    //public enum Valikko { MenuPlayer1, MenuPlayer2, MenuPlayer3, MenuPlayer4 }

    //public Valikko valikko;


    void Start()
    {
        //PITKÄN KAAVAN STARTTI
        if (currentMenu == null)
        {
            menuInitial = GameObject.Find("MenuInitial").GetComponent<Menu>();
            currentMenu = menuInitial;
        }
        if (currentMenu == menuInitial)
        {
            if (this != menuInitial)
            {
                this.gameObject.SetActive(false);
                Debug.Log("Suljettiin Menu, koska ei ollut menuInitial");
            }
        }

        //LYHYEN KAAVAN STARTTI

    }

    public void BackMenu()
    {
        if (currentMenu.parentMenu != null)
        {
            currentMenu.parentMenu.OpenMenu();
        }
        Debug.Log("Kutsuttiin BackMenu()");
    }

    public void OpenMenu()
    {
        currentMenu.gameObject.SetActive(false);
        gameObject.SetActive(true);
        currentMenu = this;
        Debug.Log("Kutsuttiin OpenMenu()");
    }

    public static void CloseMenu()
    {
        currentMenu.gameObject.SetActive(false);
        Debug.Log("Kutsuttiin CloseMenu()");
    }

    public static void OpenInitialMenu()
    {
        currentMenu = menuInitial;
        currentMenu.gameObject.SetActive(true);
        Debug.Log("Kutsuttiin OpenInitialMenu()");
    }


}